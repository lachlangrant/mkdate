# MKDate

[![CI Status](http://img.shields.io/travis/lachlangrant/MKDate.svg?style=flat)](https://travis-ci.org/lachlangrant/MKDate)
[![Version](https://img.shields.io/cocoapods/v/MKDate.svg?style=flat)](http://cocoapods.org/pods/MKDate)
[![License](https://img.shields.io/cocoapods/l/MKDate.svg?style=flat)](http://cocoapods.org/pods/MKDate)
[![Platform](https://img.shields.io/cocoapods/p/MKDate.svg?style=flat)](http://cocoapods.org/pods/MKDate)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKDate is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKDate'
```

## Author

lachlangrant, lachlangrant@rbvea.co

## License

MKDate is available under the MIT license. See the LICENSE file for more info.
