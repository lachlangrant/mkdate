//
//  Date+Bundle.swift
//  MKDate
//
//  Created by Lachlan Grant on 26/12/17.
//

import Foundation

public extension Bundle {
    
    class func dateToolsBundle() -> Bundle {
        let assetPath = Bundle(for: Constants.self).resourcePath!
        return Bundle(path: NSString(string: assetPath).appendingPathComponent("MKDate.bundle"))!
    }
}
